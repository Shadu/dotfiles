Config { font = "xft:Roboto:size=12:bold"
       , additionalFonts =
          [ "xft:FontAwesome 6 Free Solid:pixelsize=14"
          , "xft:FontAwesome:pixelsize=10:bold"
          , "xft:FontAwesome 6 Free Solid:pixelsize=16"
          , "xft:Hack Nerd Font Mono:pixelsize=21"
          , "xft:Hack Nerd Font Mono:pixelsize=25"
          ]
       , border = NoBorder
       , bgColor = "#2B2E37"
       , fgColor = "#929AAD"
       , alpha = 255
       , position = TopSize L 100 40
       , textOffset = 24
       , textOffsets = [ 25, 24 ]
       , lowerOnStart = True
       , allDesktops = False 
       , persistent = False
       , hideOnStart = False
       , iconRoot = ".config/xmonad/xmobar/icons/"
       , commands =
         [ Run UnsafeXPropertyLog "_XMONAD_LOG_1"
         , Run Date "%a, %d %b   <fn=1></fn>   %H:%M:%S" "date" 10
         , Run Memory ["-t","Mem: <fc=#AAC0F0><usedratio></fc>%"] 10
         , Run Volume "default" "Master" [] 10
         , Run Com "/home/shadu/.config/xmonad/xmobar/cpu_temp.sh" [] "cpu" 10
         , Run Com "/home/shadu/.config/xmonad/xmobar/gpu_temp.sh" [] "gpu" 10
         , Run Com "/home/shadu/.config/xmonad/xmobar/trayer-padding.sh" [] "trayerpad" 10
         ]
       , sepChar = "%"
       , alignSep = "}{"
       , template = "\
            \    \
            \%_XMONAD_LOG_1%\
            \}\
            \<action=xdotool key super+r>%date%</action>\
            \{\
            \<action=`xdotool key XF86AudioMute` button=1>\
            \<action=`xdotool key XF86AudioLowerVolume` button=5>\
            \<action=`xdotool key XF86AudioRaiseVolume` button=4>\
            \%default:Master%\
            \</action></action></action>\
            \<action=xdotool key super+y>\
            \     \
            \|\
            \     \
            \%cpu%\
            \     \
            \|\
            \     \
            \%memory%\
            \     \
            \|\
            \     \
            \%gpu%\
            \       \
            \</action>\
            \"
       }
