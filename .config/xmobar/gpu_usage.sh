#!/bin/sh

usage=$(nvidia-smi | grep 'Default' | awk '{print $13}' | sed 's/%//')
usage=${usage%???}

echo "<fc=#AAC0F0>$usage</fc>%"
