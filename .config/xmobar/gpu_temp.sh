#!/bin/sh

temp1=70
temp2=85

temp=$(nvidia-smi | grep 'Default' | awk '{print $3}' | sed 's/C//')
temp=${temp%???}

if [ "$temp" -ge "$temp2" ] ; then
    echo "<fc=#a9a1e1><fn=3></fn></fc> <fc=#C1514E>$temp</fc>°C"
elif [ "$temp" -ge "$temp1" ] ; then
    echo "<fc=#a9a1e1><fn=3></fn></fc> <fc=#C1A24E>$temp</fc>°C"
else
    echo "<fc=#a9a1e1><fn=3></fn></fc> <fc=#AAC0F0>$temp</fc>°C"

fi
