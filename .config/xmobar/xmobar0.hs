Config { font = "xft:Roboto:size=12:bold"
       , additionalFonts =
          [ "xft:Font Awesome 6 Free Solid:pixelsize=14"
          , "xft:Font Awesome:pixelsize=10:bold"
          , "xft:Font Awesome 6 Free Solid:pixelsize=16"
          , "xft:Hack Nerd Font Mono:pixelsize=21"
          , "xft:Hack Nerd Font Mono:pixelsize=25"
          ]
       , border = NoBorder
       , bgColor = "#2B2E37"
       , fgColor = "#929AAD"
       , alpha = 255
       , position = TopSize L 100 40
       , textOffset = 24
       , textOffsets = [ 25, 24 ]
       , lowerOnStart = True
       , allDesktops = False 
       , persistent = False
       , hideOnStart = False
       , iconRoot = "/home/shadu/.config/xmonad/xmobar/icons/"
       , commands =
         [ Run UnsafeXPropertyLog "_XMONAD_LOG_0"
         , Run Date "%a, %d %b   <fn=1></fn>   %H:%M:%S" "date" 10
         , Run Memory ["-t","<fc=#a9a1e1><fn=3>\xf538</fn></fc> <fc=#AAC0F0><usedratio></fc>%", "-L", "40", "-H", "60", "-l", "lightblue", "-n", "gray90", "-h", "red"] 10
         , Run Cpu [ "--template", "<fc=#a9a1e1><fn=3>\xf2db</fn></fc>  <total>%", "-L", "3", "-H", "70", "--low", "gray", "--normal", "gray", "--high", "red"] 10
         , Run CoreTemp ["-t", "<core0>°C", "-L", "40", "-H", "60", "-l", "lightblue", "-n", "gray90", "-h", "red"] 10
         --, Run Com "/home/shadu/Scripts/insideTempXmonad.py" [] "insideTemp" 100
         , Run Com "/home/shadu/.config/xmonad/xmobar/gpu_temp.sh" [] "gpu" 10
         , Run Volume "default" "Master" [] 10
         , Run Com "/home/shadu/.config/xmonad/xmobar/bluetooth.sh" [] "bluetooth" 10
         , Run Com "/home/shadu/.config/xmonad/xmobar/trayer-padding.sh" [] "trayerpad" 10
         , Run Com "/home/shadu/.config/xmonad/xmobar/gpu_usage.sh" [] "gpu_usage" 10
         ]
       , sepChar = "%"
       , alignSep = "}{"
       , template = "\
            \    \
            \%_XMONAD_LOG_0%\
            \}\
            \%date%\
            \{\
            \<action=`xdotool key XF86AudioMute` button=1>\
            \<action=`xdotool key XF86AudioLowerVolume` button=5>\
            \<action=`xdotool key XF86AudioRaiseVolume` button=4>\
            \%default:Master%\
            \</action></action></action>\
            \     \
            \|\
            \     \
            \%cpu%\
            \  \
            \%coretemp%\
            \     \
            \|\
            \     \
            \%memory%\
            \     \
            \|\
            \     \
            \%gpu%\
            \  \
            \%gpu_usage%\
            \     \
            \|\
            \     \
            \%trayerpad%"
       }
