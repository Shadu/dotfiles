
{-# OPTIONS_GHC -Wno-name-shadowing #-}
{-# OPTIONS_GHC -Wno-missing-signatures #-}
import XMonad
import System.Exit
import Prelude hiding (log)
import qualified XMonad.StackSet as W
import qualified Data.Map as M
import Data.Maybe (fromJust)
import Data.Semigroup
import Data.Bits (testBit)
import Control.Monad (unless, when)
import Foreign.C (CInt)
import Data.Foldable (find)
import Data.Monoid
import XMonad.Hooks.DynamicProperty
import XMonad.Hooks.DynamicLog
import XMonad.Hooks.InsertPosition (insertPosition, Focus(Newer), Position (Master, End))
import XMonad.Hooks.ManageDocks
import XMonad.Hooks.EwmhDesktops
import XMonad.Hooks.WindowSwallowing
import XMonad.Hooks.StatusBar
import XMonad.Hooks.ManageHelpers (isFullscreen, isDialog, doFullFloat, doRectFloat, doCenterFloat, doSink)
import XMonad.Hooks.RefocusLast (isFloat)

import XMonad.Layout.Spacing (Spacing, spacingRaw, Border (Border))
import XMonad.Layout.GridVariants (Grid(Grid))
import XMonad.Layout.NoBorders -- (smartBorders, withBorder)
import XMonad.Layout.Named (named)
import XMonad.Layout.Decoration (ModifiedLayout)
import XMonad.Layout.DraggingVisualizer (draggingVisualizer)
import XMonad.Layout.MultiToggle.Instances (StdTransformers (NBFULL, MIRROR, NOBORDERS))
import XMonad.Layout.LimitWindows (limitWindows, increaseLimit, decreaseLimit)
import XMonad.Layout.MultiToggle (EOT (EOT), Toggle (Toggle), mkToggle, (??), single)
import XMonad.Layout.LayoutModifier
import qualified XMonad.Layout.ToggleLayouts as T (toggleLayouts, ToggleLayout(Toggle))
import XMonad.Layout.MouseResizableTile
import XMonad.Layout.ResizableTile
import XMonad.Layout.Tabbed
import XMonad.Layout.IndependentScreens
import XMonad.Layout.Renamed
-- import XMonad.Layout.HintedGrid
import XMonad.Layout.PerWorkspace
import XMonad.Layout.Simplest
import XMonad.Layout.SubLayouts
import XMonad.Layout.WindowNavigation
import XMonad.Layout.WindowArranger (windowArrange, WindowArrangerMsg(..))
import qualified XMonad.Layout.MultiToggle as MT (Toggle(..))

import XMonad.Util.NamedScratchpad
import XMonad.Util.Loggers (logLayoutOnScreen, logTitleOnScreen, shortenL, wrapL, xmobarColorL)
import XMonad.Util.EZConfig (additionalKeysP)
import qualified XMonad.Util.ExtensibleState as XS
import XMonad.Util.DebugWindow
import XMonad.Util.SpawnOnce
import XMonad.Util.Hacks

import XMonad.Actions.CycleWS
import XMonad.Actions.TiledWindowDragging
import qualified XMonad.Actions.FlexibleResize as Flex
import XMonad.Actions.UpdatePointer (updatePointer)
import XMonad.Actions.OnScreen (onlyOnScreen)
import XMonad.Actions.Warp (warpToScreen)
import XMonad.Actions.RotSlaves (rotSlavesDown, rotAllDown)
import XMonad.Actions.MouseResize
import Data.List
import qualified Data.List as L


------------------------------------------------------------------
-- My Strings
------------------------------------------------------------------

myTerminal :: String
myTerminal = "kitty"          -- Default terminal

myModMask :: KeyMask
myModMask = mod4Mask          -- Super Key (--mod4Mask= super key --mod1Mask= alt key --controlMask= ctrl key --shiftMask= shift key)

grey1, grey2, grey3, grey4, cyan, orange :: String
grey1  = "#2B2E37"
grey2  = "#555E70"
grey3  = "#697180"
grey4  = "#8691A8"
cyan   = "#8BABF0"
orange = "#C45500"

myBorderWidth :: Dimension
myBorderWidth = 0             -- Window border

------------------------------------------------------------------------
-- Space between Tiling Windows
------------------------------------------------------------------------
mySpacing :: Integer -> l a -> XMonad.Layout.LayoutModifier.ModifiedLayout Spacing l a
mySpacing i = spacingRaw False (Border 5 5 5 5) True (Border 5 5 5 5) True

------------------------------------------------------------------------
-- Layout Hook
------------------------------------------------------------------------
myLayoutHook = avoidStruts $ mouseResize $ windowArrange $ T.toggleLayouts full
               $ mkToggle (NBFULL ?? NOBORDERS ?? MIRROR ?? EOT) myDefaultLayout
             where
               myDefaultLayout =      withBorder myBorderWidth tall
                                  ||| full
                                  ||| grid
                                  -- ||| mirror

------------------------------------------------------------------------
-- Tiling Layouts
------------------------------------------------------------------------
tall     = named "tall"
           $ smartBorders
           $ windowNavigation
           $ subLayout [] (smartBorders Simplest)
           $ limitWindows 8
           $ mySpacing 5
           $ ResizableTall 1 (3/100) (1/2) []               
grid     = named "grid"
           -- $ smartBorders
           -- $ windowNavigation
           -- $ subLayout [] (smartBorders Simplest)
           $ limitWindows 12
           $ mySpacing 5
           $ mkToggle (single MIRROR)
           $ Grid (16/10)   
--mirror   = named "mirror"
--           $ smartBorders
--           $ windowNavigation
--           $ subLayout [] (smartBorders Simplest)
--           $ limitWindows 6
--           $ mySpacing 5
--           $ Mirror  
--           $ ResizableTall 1 (3/100) (1/2) []            
full     = named "full"
           $ Full                     
------------------------------------------------------------------
-- Workspaces
------------------------------------------------------------------

myWorkspaces :: [[Char]] 
myWorkspaces = ["1","2","3","4","5","6","7","8","9"]

currentScreen :: X ScreenId
currentScreen = gets (W.screen . W.current . windowset)

isOnScreen :: ScreenId -> WindowSpace -> Bool
isOnScreen s ws = s == unmarshallS (W.tag ws)

workspaceOnCurrentScreen :: WSType
workspaceOnCurrentScreen = WSIs $ do
  s <- currentScreen
  return $ \x -> W.tag x /= "NSP" && isOnScreen s x

actionPrefix, actionButton, actionSuffix :: [Char]
actionPrefix = "<action=`xdotool key super+"
actionButton = "` button="
actionSuffix = "</action>"

addActions :: [(String, Int)] -> String -> String
addActions [] ws = ws
addActions (x:xs) ws = addActions xs (actionPrefix ++ k ++ actionButton ++ show b ++ ">" ++ ws ++ actionSuffix)
    where k = fst x
          b = snd x

myKeys :: XConfig l -> M.Map (KeyMask, KeySym) (X ())
myKeys conf@XConfig {XMonad.modMask = modm} = M.fromList $
 [((m .|. modm, k), windows $ onCurrentScreen f i)
 | (i, k) <- zip (workspaces' conf) [xK_1 .. xK_9]
 , (f, m) <- [(W.view, 0), (W.shift, shiftMask)]
 ]

------------------------------------------------------------------------
-- Custom Keys
------------------------------------------------------------------------
myAdditionalKeys :: [(String, X ())]
myAdditionalKeys =

    [
    -- Xmonad
        ("M-C-r", spawn "killall xmobar; ~/.xmonad/xmonad-x86_64-linux --recompile && ~/.xmonad/xmonad-x86_64-linux --restart")                   -- Recompile & Restarts xmonad
      , ("M-C-q", spawn "wlogout")
      -- , ("M-C-q", io exitSuccess)                                                   -- Quits xmonad

    -- System
      , ("<XF86AudioMute>",         spawn "amixer set Master toggle")               -- Mute
      , ("<XF86AudioRaiseVolume>",  spawn "amixer set Master 5%+ unmute")           -- Volume Up
      , ("<XF86AudioLowerVolume>",  spawn "amixer set Master 5%- unmute")           -- Volume Down
      , ("<XF86AudioPlay>",         spawn "mpc toggle")                             -- ncmpcpp play/pause
      , ("<XF86AudioStop>",         spawn "mpc pause")                              -- ncmpcpp stop
      , ("<XF86AudioPrev>",         spawn "mpc prev")                               -- ncmpcpp previous
      , ("<XF86AudioNext>",         spawn "mpc next")                               -- ncmpcpp next
      , ("<Print>",                 spawn "flameshot gui")                          -- Screenshot select area
      , ("C-<Print>",                spawn "flameshot screen -c")                   -- Screenshot Current Screen

    -- Run Prompt
      , ("M-p", spawn "dmenu_run")                                                  -- Run Dmenu (rofi -show drun)
      , ("M-d", spawn "rofi -show drun -theme ~/.config/rofi/style_9.rasi")                                            -- Rofi Launcher

    -- Apps
      , ("M-v", spawn $ myTerminal ++ " --title Nvim -e nvim")
      , ("M-t", spawn myTerminal)                                                   -- Terminal
      , ("M-f", spawn "firefox")                                                    -- Firefox
      , ("M-e", spawn "thunar")                                                     -- Thunar  
      , ("M-S-e", spawn "emacsclient --create-frame --alternate-editor=''")
      , ("M-S-c", spawn "kitty cmus")
      --, ("M-f", spawn "kitty ranger")                                               -- Ranger

    -- Window navigation
      , ("M-<Space>", sendMessage NextLayout)                                       -- Rotate through the available layout algorithms
      --, ("M-S-<Left>", windows W.swapMaster)                                          -- Swap the focused window and the master window
      , ("M-<Up>", windows W.swapUp)                                                -- Swap the focused window with the previous window
      , ("M-<Down>", windows W.swapDown)                                            -- Swap the focused window with the next window     
      , ("M-C-<Tab>", rotSlavesDown)                                                -- Rotate all windows except master and keep focus in place
      , ("M-S-C-<Tab>", rotAllDown)                                                     -- Rotate all windows
      , ("M-h", sendMessage Shrink)
      , ("M-l", sendMessage Expand)
      , ("M-C-f", sendMessage (MT.Toggle NBFULL) >> sendMessage ToggleStruts)        -- Toggles full width
      , ("M-S-q", kill)                                                              -- Quit the currently focused client
      --, ("M-S-q", killAll)                                                          -- Quit all windows on current workspace
      , ("M-S-<Space>", withFocused toggleFloat)                                    -- Toggle float on focused window
      , ("M1-<Tab>", spawn "rofi -show window -theme ~/.config/rofi/style_9")
      
      , ("M-S-<Tab>", moveTo Prev workspaceOnCurrentScreen)
      , ("M-<Tab>", moveTo Next workspaceOnCurrentScreen)

      -- screen controll
      , ("M-S-<Right>", shiftNextScreen)

      -- Picom Toggle
      , ("M-S-<F10>", spawn "~/.config/scripts/toggle_comp.sh")

    -- Scratchpads
      , ("M-S-s", withFocused $ toggleDynamicNSP "dyn1")
      , ("M-s"  , dynamicNSPAction "dyn1")

      , ("M-S-i", spawn "killall trayer; trayer --monitor 0 --edge top --align right --widthtype request --padding 5 --iconspacing 5 --SetDockType true --SetPartialStrut true --expand true --transparent true --alpha 0 --tint 0x2B2E37  --height 20 --distance 10 &")
      ]

------------------------------------------------------------------------
-- Floats
------------------------------------------------------------------------
-- (~?) :: Eq a => Query [a] -> [a] -> Query Bool
-- q ~? x = fmap (x `L.isInfixOf`) q

(/=?) :: Eq a => Query a -> a -> Query Bool
q /=? x = fmap (/= x) q

(~?) :: (Eq a, Functor m) => m [a] -> [a] -> m Bool
q ~? x = fmap (x `isInfixOf`) q

myManageHook = composeAll
     [ className =? "confirm"                           --> doFloat
     , className =? "file_progress"                     --> doFloat
     --, className =? "mpv"                               --> doCenterFloat
     , className =? "dialog"                            --> doFloat
     , className =? "download"                          --> doFloat
     , className =? "error"                             --> doFloat
     , className =? "notification"                      --> doFloat
     , className =? "toolbar"                           --> doFloat
     , title     ~? "Steam - Self Updater"              --> doFloat
     , title     =? "Friends List"                      --> doFloat
     , title     ~? "Steam - News"                      --> doCenterFloat
     , appName   =? "pavucontrol"                       --> doFloat
     , title     =? "Helvum - Pipewire Patchbay"        --> doFloat
     , isFullscreen -->  doFullFloat
     , isDialog --> doCenterFloat
     ] <+> manageDocks -- <+> namedScratchpadManageHook myScratchPads

myHandleEventHook :: Event -> X All
myHandleEventHook = swallowEventHook (className =? "kitty") (return True) 
                  -- <+> multiScreenFocusHook

------------------------------------------------------------------------
-- Toggle Floats
------------------------------------------------------------------------

toggleFloat :: Window -> X ()
toggleFloat w =
  windows
    ( \s ->
        if M.member w (W.floating s)
          then W.sink w s
          else (W.float w (W.RationalRect (1 / 3) (1 / 4) (1 / 2) (1 / 2)) s)
    )

------------------------------------------------------------------------
-- Startup Hooks
------------------------------------------------------------------------

myStartupHook = do
    spawn "killall trayer; trayer --monitor 0 --edge top --align right --widthtype request --padding 5 --iconspacing 5 --SetDockType true --SetPartialStrut true --expand true --transparent true --alpha 0 --tint 0x2B2E37  --height 20 --distance 10 &"
    modify $ \xstate -> xstate { windowset = onlyOnScreen 1 "1_1" (windowset xstate) }
    spawn "$HOME/.xmonad/scripts/autostart.sh"

------------------------------------------------------------------
-- Follow Mouse Focus
------------------------------------------------------------------
myMouseBindings :: XConfig l -> M.Map (KeyMask, Button) (Window -> X ())
myMouseBindings XConfig {XMonad.modMask = modm} = M.fromList
  [ ((modm, button1), \w -> focus w >> mouseMoveWindow w >> windows W.shiftMaster)
  , ((modm .|. shiftMask, button1), dragWindow)
  , ((modm, button3), \w -> focus w >> Flex.mouseResizeWindow w)
  ]

newtype MyUpdatePointerActive = MyUpdatePointerActive Bool
instance ExtensionClass MyUpdatePointerActive where
  initialValue = MyUpdatePointerActive True

myUpdatePointer :: (Rational, Rational) -> (Rational, Rational) -> X ()
myUpdatePointer refPos ratio =
  whenX isActive $ do
    dpy <- asks display
    root <- asks theRoot
    (_,_,_,_,_,_,_,m) <- io $ queryPointer dpy root
    unless (testBit m 9 || testBit m 8 || testBit m 10) $ -- unless the mouse is clicking
      updatePointer refPos ratio

  where
    isActive = (\(MyUpdatePointerActive b) -> b) <$> XS.get

multiScreenFocusHook :: Event -> X All
multiScreenFocusHook MotionEvent { ev_x = x, ev_y = y } = do
  ms <- getScreenForPos x y
  case ms of
    Just cursorScreen -> do
      let cursorScreenID = W.screen cursorScreen
      focussedScreenID <- gets (W.screen . W.current . windowset)
      when (cursorScreenID /= focussedScreenID) (focusWS $ W.tag $ W.workspace cursorScreen)
      return (All True)
    _ -> return (All True)
  where getScreenForPos :: CInt -> CInt
            -> X (Maybe (W.Screen WorkspaceId (Layout Window) Window ScreenId ScreenDetail))
        getScreenForPos x y = do
          ws <- windowset <$> get
          let screens = W.current ws : W.visible ws
              inRects = map (inRect x y . screenRect . W.screenDetail) screens
          return $ fst <$> find snd (zip screens inRects)
        inRect :: CInt -> CInt -> Rectangle -> Bool
        inRect x y rect = let l = fromIntegral (rect_x rect)
                              r = l + fromIntegral (rect_width rect)
                              t = fromIntegral (rect_y rect)
                              b = t + fromIntegral (rect_height rect)
                           in x >= l && x < r && y >= t && y < b
        focusWS :: WorkspaceId -> X ()
        focusWS ids = windows (W.view ids)
multiScreenFocusHook _ = return (All True)

------------------------------------------------------------------
-- XMobar
------------------------------------------------------------------

myWorkspaceIndices :: M.Map [Char] Integer
myWorkspaceIndices = M.fromList $ zip myWorkspaces [1..]

clickable :: [Char] -> [Char] -> [Char]
clickable icon ws = addActions [ (show i, 1), ("q", 2), ("Left", 4), ("Right", 5) ] icon
                    where i = fromJust $ M.lookup ws myWorkspaceIndices

myStatusBarSpawner :: Applicative f => ScreenId -> f StatusBarConfig
myStatusBarSpawner (S s) = do
                    pure $ statusBarPropTo ("_XMONAD_LOG_" ++ show s)
                          ("xmobar -x " ++ show s ++ " ~/.config/xmonad/xmobar/xmobar" ++ show s ++ ".hs")
                          (pure $ myXmobarPP (S s))


myXmobarPP :: ScreenId -> PP
myXmobarPP s  = filterOutWsPP [scratchpadWorkspaceTag] . marshallPP s $ def
  { ppSep = ""
  , ppWsSep = ""
  , ppCurrent = xmobarColor cyan "" . clickable wsIconFull
  , ppVisible = xmobarColor grey4 "" . clickable wsIconFull
  , ppVisibleNoWindows = Just (xmobarColor grey4 "" . clickable wsIconFull)
  , ppHidden = xmobarColor grey2 "" . clickable wsIconHidden
  , ppHiddenNoWindows = xmobarColor grey2 "" . clickable wsIconEmpty
  , ppUrgent = xmobarColor orange "" . clickable wsIconFull
  , ppOrder = \(ws : _ : _ : extras) -> ws : extras
  , ppExtras  = [ wrapL (actionPrefix ++ "n" ++ actionButton ++ "1>") actionSuffix
                $ wrapL (actionPrefix ++ "q" ++ actionButton ++ "2>") actionSuffix
                $ wrapL (actionPrefix ++ "Left" ++ actionButton ++ "4>") actionSuffix
                $ wrapL (actionPrefix ++ "Right" ++ actionButton ++ "5>") actionSuffix
                $ wrapL "    " "    " $ layoutColorIsActive s (logLayoutOnScreen s)
                , wrapL (actionPrefix ++ "q" ++ actionButton ++ "2>") actionSuffix
                $  titleColorIsActive s (shortenL 81 $ logTitleOnScreen s)
                ]
  }
  where
    wsIconFull   = "  <fn=2>\xf111</fn>   "
    wsIconHidden = "  <fn=2>\xf111</fn>   "
    wsIconEmpty  = "  <fn=2>\xf10c</fn>   "
    titleColorIsActive n l = do
      c <- withWindowSet $ return . W.screen . W.current
      if n == c then xmobarColorL cyan "" l else xmobarColorL grey3 "" l
    layoutColorIsActive n l = do
      c <- withWindowSet $ return . W.screen . W.current
      if n == c then wrapL "<icon=/home/shadu/.config/xmonad/xmobar/icons/" "_selected.xpm/>" l else wrapL "<icon=/home/shadu/.config/xmonad/xmobar/icons/" ".xpm/>" l

------------------------------------------------------------------
-- Dock Restacking
------------------------------------------------------------------

logH :: X ()
logH    = do
    trace "Tree..."
    d <- asks display
    r <- asks theRoot
    (_, _, ts) <- liftIO $ queryTree d r
    ls <- mapM debugWindow ts
    trace (intercalate "\n" ls)

-- | Restack dock under lowest managed window.
lowerDock :: ManageHook
lowerDock = checkDock --> do
    w <- ask
    mlw <- liftX $ findLowest
    case mlw of
      Just lw   -> liftX $ do
        d <- asks display
        s <- debugWindow lw
        trace "_Before_ restacking dock:"
        logH
        trace $ "Lowest managed window: " ++ s
        liftIO $ restackWindows d [lw, w]
        trace "_After_ restack dock:"
        logH
        return idHook
      Nothing   -> return idHook

-- | Find lowest managed window.
findLowest :: X (Maybe Window)
findLowest  = withWindowSet $ \ws -> do
    d <- asks display
    r <- asks theRoot
    (_, _, ts) <- liftIO $ queryTree d r
    return (find (`W.member` ws) ts)




------------------------------------------------------------------
-- Main
------------------------------------------------------------------

main :: IO ()
main = xmonad
       . ewmh
       . ewmhFullscreen
       . dynamicSBs myStatusBarSpawner
       . docks
       $ def
        { focusFollowsMouse  = True
        , clickJustFocuses   = False 
        , borderWidth        = 0
        , modMask            = mod4Mask
        , normalBorderColor  = grey2
        , focusedBorderColor = cyan
        , terminal           = myTerminal
        , keys               = myKeys
        --, workspaces         = myWorkspaces    
        , workspaces         = withScreens 2 myWorkspaces
        , layoutHook         = myLayoutHook
        , manageHook         = myManageHook <+> lowerDock
        , startupHook        = myStartupHook
        , mouseBindings      = myMouseBindings

        , rootMask = rootMask def .|. pointerMotionMask
        --  , logHook            = logHook def <+> myUpdatePointer (0.75, 0.75) (0, 0)
        , handleEventHook    = myHandleEventHook <+> trayerAboveXmobarEventHook
        } `additionalKeysP` myAdditionalKeys
