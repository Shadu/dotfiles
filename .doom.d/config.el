;;; $DOOMDIR/config.el -*- lexical-binding: t; -*-

;; Place your private configuration here! Remember, you do not need to run 'doom
;; sync' after modifying this file!


;; Some functionality uses this to identify you, e.g. GPG configuration, email
;; clients, file templates and snippets.
(setq user-full-name "John Doe"
      user-mail-address "john@doe.com")

;; Doom exposes five (optional) variables for controlling fonts in Doom. Here
;; are the three important ones:
;;
;; + `doom-font'
;; + `doom-variable-pitch-font'
;; + `doom-big-font' -- used for `doom-big-font-mode'; use this for
;;   presentations or streaming.
;;
;; They all accept either a font-spec, font string ("Input Mono-12"), or xlfd
;; font string. You generally only need these two:
 (setq doom-font (font-spec :family "monospace" :size 14 :weight 'semi-light)
       doom-variable-pitch-font (font-spec :family "sans" :size 14))

;; There are two ways to load a theme. Both assume the theme is installed and
;; available. You can either set `doom-theme' or manually load a theme with the
;; `load-theme' function. This is the default:
;; Default theme: (setq doom-theme 'doom-one)
(setq doom-theme 'doom-one)

;; If you use `org' and don't want your org files in the default location below,
;; change `org-directory'. It must be set before org loads!
(setq org-directory "~/org/")

;; This determines the style of line numbers in effect. If set to `nil', line
;; numbers are disabled. For relative line numbers, set this to `relative'.
(setq display-line-numbers-type t)


;; Here are some additional functions/macros that could help you configure Doom:
;;
;; - `load!' for loading external *.el files relative to this one
;; - `use-package!' for configuring packages
;; - `after!' for running code after a package has loaded
;; - `add-load-path!' for adding directories to the `load-path', relative to
;;   this file. Emacs searches the `load-path' when you load packages with
;;   `require' or `use-package'.
;; - `map!' for binding new keys
;;
;; To get information about any of these functions/macros, move the cursor over
;; the highlighted symbol at press 'K' (non-evil users must press 'C-c c k').
;; This will open documentation for it, including demos of how they are used.
;;
;; You can also try 'gd' (or 'C-c c d') to jump to their definition and see how
;; they are implemented.
;;(add-hook 'after-init-hook #'neotree-show)

;;(setq lsp-clients-clangd-args '("-j=3"
;;				"--background-index"
;;				"--clang-tidy"
;;				"--completion-style=detailed"
;;				"--header-insertion=never"
;;				"--header-insertion-decorators=0"))
;;(after! lsp-clangd (set-lsp-priority! 'clangd 1))
(setq projectile-switch-project-action #'+treemacs/toggle)
(after! ccls
  (setq ccls-initialization-options '(:index (:comments 2) :completion (:detailedLabel t)))
  (set-lsp-priority! 'ccls 2)) ; optional as ccls is the default in Doom

; (setq doom-init-ui-hook (+treemacs/toggle))
(projectile-mode +1)
(evil-commentary-mode)
;;(tree-sitter-mode +1)
;; Recommended keymap prefix on Windows/Linux
(define-key projectile-mode-map (kbd "C-c p") 'projectile-command-map)

(add-hook! (prog-mode text-mode) 
     (defun my/activate-platformio-if-h ()
          (cond ((string-match-p "PlatformIO" (or (buffer-file-name) "nope"))
                 (platformio-mode +1)))))

(add-hook! (prog-mode text-mode)
           (run-with-timer 1 nil (lambda () (save-selected-window (treemacs-display-current-project-exclusively)))))

(with-eval-after-load 'treemacs
  (defun treemacs-ignore-gitignore (file _)
    (string= file ".gitignore"))
  (push #'treemacs-ignore-gitignore treemacs-ignored-file-predicates))

(with-eval-after-load 'treemacs
  (defun treemacs-ignore-ccls (file _)
    (string= file ".ccls-cache"))
  (push #'treemacs-ignore-ccls treemacs-ignored-file-predicates))

(with-eval-after-load 'treemacs
  (defun treemacs-ignore-vscode (file _)
    (string= file ".vscode"))
  (push #'treemacs-ignore-vscode treemacs-ignored-file-predicates))

(with-eval-after-load 'treemacs
  (defun treemacs-ignore-pio (file _)
    (string= file ".pio"))
  (push #'treemacs-ignore-pio treemacs-ignored-file-predicates))

(require 'dap-python)

(defvar default-ecb-source-path (list '("~/Documents/workspace" "Workspace")
								 '("~/" "~/")
								 '("/" "/")))

(setq projectile-project-search-path '("~/Documents/Projects/" "~/Documents/PlatformIO/"))

(add-hook 'ecb-basic-buffer-sync-hook
		  (lambda ()
			(when (functionp 'projectile-get-project-directories)
			  (when (projectile-project-p)
				(dolist (path-dir (projectile-get-project-directories))
				  (unless (member (list path-dir path-dir) default-ecb-source-path)
					(push (list path-dir path-dir) default-ecb-source-path)
					(customize-set-variable 'ecb-source-path default-ecb-source-path)
					))))))
